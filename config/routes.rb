Rails.application.routes.draw do
  resources :akses_moduls
  resources :moduls
  resources :users
  resources :user_roles
  
  get 'shortcut', to: 'shortcut#index'
  get 'template', to: 'template#index'
  get 'user_roles', to: 'user_roles#index'
  get 'akses_moduls', to: 'akses_moduls#index'
  get 'moduls', to: 'moduls#index'
  get 'users', to: 'users#index'
  root 'dashboard#index'
end
