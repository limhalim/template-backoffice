class AksesModulsController < ApplicationController
  before_action :set_akses_modul, only: %i[ show edit update destroy ]

  # GET /akses_moduls or /akses_moduls.json
  def index
    @akses_moduls = AksesModul.all
  end

  # GET /akses_moduls/1 or /akses_moduls/1.json
  def show
  end

  # GET /akses_moduls/new
  def new
    @akses_modul = AksesModul.new
  end

  # GET /akses_moduls/1/edit
  def edit
  end

  # POST /akses_moduls or /akses_moduls.json
  def create
    @akses_modul = AksesModul.new(akses_modul_params)

    respond_to do |format|
      if @akses_modul.save
        format.html { redirect_to akses_modul_url(@akses_modul), notice: "Akses modul was successfully created." }
        format.json { render :show, status: :created, location: @akses_modul }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @akses_modul.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /akses_moduls/1 or /akses_moduls/1.json
  def update
    respond_to do |format|
      if @akses_modul.update(akses_modul_params)
        format.html { redirect_to akses_modul_url(@akses_modul), notice: "Akses modul was successfully updated." }
        format.json { render :show, status: :ok, location: @akses_modul }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @akses_modul.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /akses_moduls/1 or /akses_moduls/1.json
  def destroy
    @akses_modul.destroy

    respond_to do |format|
      format.html { redirect_to akses_moduls_url, notice: "Akses modul was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_akses_modul
      @akses_modul = AksesModul.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def akses_modul_params
      params.require(:akses_modul).permit(:modul_id, :user_role_id)
    end
end
