class AksesModul < ApplicationRecord
    belongs_to :user_role
    belongs_to :modul

    delegate :name_roles, to: :user_role, prefix: true, allow_nil: true 
    delegate :nama_modul, to: :modul, prefix: true, allow_nil: true 
end
