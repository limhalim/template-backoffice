class User < ApplicationRecord
    belongs_to :user_role

    delegate :name_roles, to: :user_role, prefix: true, allow_nil: true 
end
