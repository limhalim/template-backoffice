json.extract! akses_modul, :id, :modul_id, :user_role_id, :created_at, :updated_at
json.url akses_modul_url(akses_modul, format: :json)
