json.extract! user_role, :id, :name_roles, :roles_level, :created_at, :updated_at
json.url user_role_url(user_role, format: :json)
