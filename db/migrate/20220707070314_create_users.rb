class CreateUsers < ActiveRecord::Migration[7.0]
  def change
    create_table :users do |t|
      t.string :name_user
      t.integer :user_role_id

      t.timestamps
    end
  end
end
