class CreateAksesModuls < ActiveRecord::Migration[7.0]
  def change
    create_table :akses_moduls do |t|
      t.integer :modul_id
      t.integer :user_role_id

      t.timestamps
    end
  end
end
