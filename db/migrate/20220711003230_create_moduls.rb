class CreateModuls < ActiveRecord::Migration[7.0]
  def change
    create_table :moduls do |t|
      t.string :nama_modul
      t.string :status
      t.string :url_modul

      t.timestamps
    end
  end
end
