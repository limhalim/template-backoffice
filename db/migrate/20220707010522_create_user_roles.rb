class CreateUserRoles < ActiveRecord::Migration[7.0]
  def change
    create_table :user_roles do |t|
      t.string :name_roles
      t.string :roles_level

      t.timestamps
    end
  end
end
